
## Frontend libs ##
TimelineMe: http://mickaelr.github.io/jquery-timelineMe/



## Python deps ##

Django==1.9.7
django-tastypie==0.13.3
python-dateutil==2.5.3
python-mimeparse==1.5.2
six==1.10.0



## Frontend deps ##

"jquery": "3.1.0",
"bootstrap": "3.3.5",
"font-awesome": "4.6.3",
"timelineme.js": "https://github.com/mickaelr/jquery-timelineMe.git#0.1.8"