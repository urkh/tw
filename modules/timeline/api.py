#from tastypie.authorization import Authorization
from django.contrib.auth.models import User
from tastypie import fields
from tastypie.resources import ModelResource
from modules.timeline.models import Timeline, TimelineContent


class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        fields = ['id', 'username', 'first_name', 'last_name']
        allowed_methods = ['get']

class TimelineResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')
    class Meta:
        queryset = Timeline.objects.all()
        resource_name = 'timeline'
        #authorization = Authorization()



class TimelineContentResource(ModelResource):
    class Meta:
        queryset = TimelineContent.objects.all()
        resource_name = 'timeline_content'
        #authorization = Authorization()