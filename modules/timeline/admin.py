from django.contrib import admin
from modules.timeline.models import Timeline, TimelineContent, Tags, Category

admin.site.register(Timeline)
admin.site.register(TimelineContent)
admin.site.register(Tags)
admin.site.register(Category)
