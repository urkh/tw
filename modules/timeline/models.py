from django.contrib.auth.models import User
from django.utils.text import slugify
from django.db import models

from tastypie.utils.timezone import now

class Timeline(models.Model):
    title = models.CharField(max_length=70)
    slug = models.SlugField(null=True, blank=True)
    user = models.ForeignKey(User, null=True, blank=True)
    pub_date = models.DateTimeField(default=now)
    category = models.ForeignKey('Category', null=True, blank=True)
    tags = models.ManyToManyField('Tags', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)[:50]

        return super(Timeline, self).save(*args, **kwargs)


class TimelineContent(models.Model):
    timeline = models.ForeignKey(Timeline)
    content = models.CharField(max_length=140)
    image = models.CharField(max_length=300, blank=True, null=True)
    extra = models.CharField(max_length=200, blank=True, null=True)
    way = models.CharField(max_length=10, null=True, blank=True, choices=(('left', 'Left'),('right', 'Right')))
    pub_date = models.DateTimeField(default=now)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return self.timeline.title


class Category(models.Model):
    name = models.CharField(max_length=100)

class Tags(models.Model):
    name = models.CharField(max_length=100)