from django.views.generic import View
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from modules.timeline.models import TimelineContent, Timeline
from modules.timeline.forms import TimelineForm

class TimelineView(View):
    form_class = TimelineForm
    template_name = "timeline/timeline.html"


    def get(self, request, *args, **kwargs):
        timeline = TimelineContent.objects.filter(timeline_id=kwargs.get('pk')).order_by('-id')
        return render(request, self.template_name, {'timeline': timeline, 'form':self.form_class})


    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            data = request.POST
            TimelineContent.objects.create(
                timeline_id=kwargs.get('pk'),
                content=data.get('content'),
                extra=data.get('content'),
                way=data.get('way')
            )
            return HttpResponseRedirect('/timeline/{}'.format(kwargs.get('pk')))

        return render(request, self.template_name, {'form': form})



class NewsView(View):
    template_name = "timeline/news.html"

    def get(self, request, *args, **kwargs):
        news = Timeline.objects.all().order_by('-id')
        return render(request, self.template_name, {'news': news})