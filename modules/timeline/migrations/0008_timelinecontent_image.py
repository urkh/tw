# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-20 23:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timeline', '0007_auto_20160720_2259'),
    ]

    operations = [
        migrations.AddField(
            model_name='timelinecontent',
            name='image',
            field=models.CharField(blank=True, max_length=300, null=True),
        ),
    ]
