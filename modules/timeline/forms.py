from django import forms

class TimelineForm(forms.Form):

    content = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ))

    extra = forms.CharField(
        required=False, 
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'rows': '3'
            }
        ))

    image = forms.CharField(
        required=False, 
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ))

    way = forms.ChoiceField(
        required=False, 
        widget=forms.RadioSelect, 
        choices=(('left', 'Left'),('right', 'Right'))
        )