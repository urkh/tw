from django.conf.urls import url, include
from django.contrib import admin
from tastypie.api import Api

from modules.timeline.api import TimelineResource, TimelineContentResource, UserResource
from modules.timeline.views import TimelineView, NewsView



## /api/v1/uri
v1_api = Api(api_name='v1')
v1_api.register(UserResource())
v1_api.register(TimelineResource())
v1_api.register(TimelineContentResource())


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(v1_api.urls)),

    url(r'^timeline/(?P<pk>[0-9]+)/$', TimelineView.as_view(), name='timeline-detail'),
    url(r'^$', NewsView.as_view()),
]
